void main() {
  var DevelopedApp = new AppDeveloper();

  DevelopedApp.appName = "Fnb";
  DevelopedApp.category = "finance";
  DevelopedApp.developer = "fnb team";
  DevelopedApp.year = 2012;

  print(DevelopedApp.appName);
  print(DevelopedApp.category);
  print(DevelopedApp.developer);
  print(DevelopedApp.year);

  DevelopedApp.getTheAppName();
}

class AppDeveloper {
  String? appName;
  String? category;
  String? developer;
  int? year;

  void getTheAppName() {
    String appName = "Fnb";
    print("App name in capital letters ${appName.toUpperCase()}");
  }
}
